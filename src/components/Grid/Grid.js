import React, { useState, useEffect } from 'react';
import Cell from '../Cell/Cell';

import { connect } from 'react-redux';
import { toggleIsEditing } from '../../store/actions/grid.actions';

import './Grid.scss';

function Grid({ grid }) {
  const [gridCells, setGridCells] = useState([[1]]);

  const renderCells = () => {
    let newGridCells = [];
    let edge = 16;

    if (grid[0].length > 1024 / 18) {
      edge = Math.round(1024 / grid[0].length);
    }

    for (let i = 0; i < grid.length; i++) {
      let row = [];

      for (let j = 0; j < grid[i].length; j++) {
        row.push(<Cell key={[i, j]} dead={!grid[i][j]} edge={edge} column={i} row={j} />);
      }

      newGridCells.push(
        <div className="Grid__row" key={i}>
          {row}
        </div>,
      );
    }

    setGridCells(newGridCells);
  };

  useEffect(renderCells, [grid]);

  return <div className="Grid">{gridCells}</div>;
}

function mapStateToProps(state) {
  const { gridArray: grid } = state.grid;
  return {
    grid,
  };
}

export default connect(mapStateToProps, { toggleIsEditing })(Grid);
