import React, { useState } from 'react';

import { connect } from 'react-redux';
import { setGridDimensions, toggleIsEditing } from '../../store/actions/grid.actions';

import './GridEdit.scss';

function GridEdit({ grid, setGridDimensions, toggleIsEditing, isEditing, isEditable }) {
  const [columns, setColumns] = useState(grid.length);
  const [rows, setRows] = useState(grid[0].length);
  const [hasError, setHasError] = useState(false);

  const onSubmit = (event) => {
    event.preventDefault();
    setGridDimensions(+columns, +rows);
    toggleIsEditing();
  };

  const onColumnsChange = (event) => {
    const { value } = event.target;

    if (value < 1 || value > 100) {
      setHasError(true);
    } else {
      setHasError(false);
    }
    setColumns(value);
  };

  const onRowsChange = (event) => {
    const { value } = event.target;

    if (value < 1 || value > 100) {
      setHasError(true);
    } else {
      setHasError(false);
    }
    setRows(value);
  };

  return (
    <div className="GridEdit">
      {isEditing ? (
        <form onSubmit={onSubmit} className="GridEdit__form">
          <div className="row GridEdit__form__row">
            <div className="GridEdit__form-group">
              <label htmlFor="rows">Rows</label>
              <input type="number" id="rows" value={rows} onChange={onRowsChange} />
            </div>
            <div className="GridEdit__form-group">
              <label htmlFor="columns">Columns</label>
              <input type="number" id="columns" value={columns} onChange={onColumnsChange} />
            </div>
            {hasError && <p className="GridEdit__error">Please enter a number between 1-100</p>}
          </div>
          <button className="button button--success" type="submit">
            Save
          </button>
          <button className="button button--danger" type="button" onClick={toggleIsEditing}>
            Close
          </button>
        </form>
      ) : (
        <button className="button" disabled={!isEditable} onClick={toggleIsEditing}>
          Edit
        </button>
      )}
    </div>
  );
}

function mapStateToProps(state) {
  const { gridArray: grid, isEditing, isEditable } = state.grid;
  return {
    grid,
    isEditing,
    isEditable,
  };
}

export default connect(mapStateToProps, { setGridDimensions, toggleIsEditing })(GridEdit);
