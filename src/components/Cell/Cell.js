import React from 'react';

import { connect } from 'react-redux';
import { toggleCell } from '../../store/actions/grid.actions';
import { classList } from '../../services/classList.service';

import './Cell.scss';

function Cell({ edge, dead, column, row, toggleCell, isEditable }) {
  const onClick = () => {
    return isEditable ? toggleCell(column, row) : null;
  };

  return (
    <div
      className={classList({
        Cell: true,
        'Cell--dead': dead,
        'Cell--editable': isEditable,
      })}
      style={{ width: edge + 'px', height: edge + 'px' }}
      onClick={onClick}
    ></div>
  );
}

function mapStateToProps(state) {
  return {
    isEditable: state.grid.isEditable,
  };
}

export default connect(mapStateToProps, { toggleCell })(Cell);
