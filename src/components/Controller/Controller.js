import React from 'react';

import { connect } from 'react-redux';
import { useInterval } from '../../hooks/useInterval';
import { runGame, nextMove, stopGame, clearAll } from '../../store/actions/grid.actions';
import { calcNewGeneration } from '../../services/grid.service';

import './Controller.scss';

function Controller({
  isEditing,
  isPlaying,
  runGame,
  nextMove,
  gridArray,
  stopGame,
  generation,
  clearAll,
}) {
  const makeMove = () => {
    const newGrid = calcNewGeneration(gridArray);
    nextMove(newGrid);
  };

  useInterval(makeMove, isPlaying ? 100 : null);

  return (
    <div className="Controller">
      {isPlaying ? (
        <button className="button button--danger" onClick={stopGame}>
          Stop
        </button>
      ) : (
        <button className="button button--success" disabled={isEditing} onClick={runGame}>
          Run
        </button>
      )}
      <button className="button button--success" disabled={isPlaying} onClick={makeMove}>
        Next
      </button>
      <button className="button button--danger" onClick={clearAll}>
        Clear all
      </button>
      <p className="Controller__generation">Generation: {generation}</p>
    </div>
  );
}

function mapStateToProps(state) {
  const { isEditing, isPlaying, gridArray, generation } = state.grid;
  return {
    gridArray,
    isEditing,
    isPlaying,
    generation,
  };
}

export default connect(mapStateToProps, { runGame, nextMove, stopGame, clearAll })(Controller);
