import React from 'react';
import Grid from './components/Grid/Grid';
import Controller from './components/Controller/Controller';
import GridEdit from './components/GridEdit/GridEdit';

function App() {
  return (
    <div className="container">
      <Controller />
      <GridEdit />
      <Grid />
    </div>
  );
}

export default App;
