export function calcNewGeneration(grid) {
  const newGrid = JSON.parse(JSON.stringify(grid));

  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[0].length; j++) {
      const liveNeighbours = countLiveNeighbours(i, j);

      if (grid[i][j] && (liveNeighbours < 2 || liveNeighbours > 3)) {
        console.log(i, j, liveNeighbours);
        newGrid[i][j] = false;
      } else if (!grid[i][j] && liveNeighbours === 3) {
        console.log(i, j, liveNeighbours);
        newGrid[i][j] = true;
      }
    }
  }

  function countLiveNeighbours(row, column) {
    let liveNeighbours = 0;
    const cellsToCheck = [
      [row - 1, column - 1],
      [row - 1, column],
      [row - 1, column + 1],
      [row, column - 1],
      [row, column + 1],
      [row + 1, column - 1],
      [row + 1, column],
      [row + 1, column + 1],
    ];

    for (let i = 0; i < cellsToCheck.length; i++) {
      const cellRow = cellsToCheck[i][0];
      const cellColumn = cellsToCheck[i][1];
      if (isInGrid(cellRow, cellColumn) && grid[cellRow][cellColumn]) {
        liveNeighbours++;
      }
    }

    return liveNeighbours;
  }

  function isInGrid(row, column) {
    return row >= 0 && row < grid.length && column >= 0 && column < grid[0].length;
  }

  return newGrid;
}
