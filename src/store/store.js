import thunk from 'redux-thunk';
import reducers from './reducers';
import { createStore, applyMiddleware, compose } from 'redux';

const __DEV__ = process.env.NODE_ENV && process.env.NODE_ENV === 'development';

export default !__DEV__
  ? createStore(reducers, applyMiddleware(thunk))
  : createStore(
      reducers,
      compose(
        applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
      ),
    );
