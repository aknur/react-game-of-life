import {
  TOGGLE_CELL,
  SET_GRID_DIMENSIONS,
  TOGGLE_IS_EDITING,
  RUN_GAME,
  STOP_GAME,
  NEXT_MOVE,
  CLEAR__ALL,
} from '../actions/grid.actions';

const initialState = {
  gridArray: Array(10).fill(Array(10).fill(false)),
  isEditable: true,
  isEditing: false,
  generation: 0,
  isPlaying: false,
};

export default function (state = initialState, action) {
  let newGridArray = JSON.parse(JSON.stringify(state.gridArray));

  switch (action.type) {
    case TOGGLE_CELL:
      const { column: i, row: j } = action.payload;
      newGridArray[i][j] = !newGridArray[i][j];
      return {
        ...state,
        gridArray: newGridArray,
      };
    case SET_GRID_DIMENSIONS:
      newGridArray = Array(action.payload.rows).fill(Array(action.payload.columns).fill(false));

      return { ...state, gridArray: newGridArray };
    case TOGGLE_IS_EDITING:
      return {
        ...state,
        isEditing: !state.isEditing,
      };
    case RUN_GAME:
      return {
        ...state,
        isEditable: false,
        isEditing: false,
        generation: 0,
        isPlaying: true,
      };
    case STOP_GAME:
      return {
        ...state,
        isEditable: true,
        isPlaying: false,
      };
    case NEXT_MOVE:
      return {
        ...state,
        gridArray: action.payload.grid,
        generation: ++state.generation,
      };
    case CLEAR__ALL:
      newGridArray = Array(newGridArray.length).fill(Array(newGridArray[0].length).fill(false));
      return {
        ...state,
        gridArray: newGridArray,
        generation: 0,
      };
    default:
      return state;
  }
}
