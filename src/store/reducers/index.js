import { combineReducers } from 'redux';

import gridReducer from './grid.reducer';

const appReducer = combineReducers({
  grid: gridReducer,
});

export default appReducer;
