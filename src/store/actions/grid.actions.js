export const TOGGLE_CELL = 'TOGGLE_CELL';
export const CLEAR__ALL = 'CLEAR__ALL';
export const SET_GRID_DIMENSIONS = 'SET_GRID_DIMENSIONS';
export const TOGGLE_IS_EDITING = 'TOGGLE_IS_EDITING';
export const RUN_GAME = 'RUN_GAME';
export const STOP_GAME = 'STOP_GAME';
export const NEXT_MOVE = 'NEXT_MOVE';

export const toggleCell = (column, row) => (dispatch) => {
  dispatch({
    type: TOGGLE_CELL,
    payload: {
      column,
      row,
    },
  });
};

export const setGridDimensions = (columns, rows) => (dispatch) => {
  dispatch({
    type: SET_GRID_DIMENSIONS,
    payload: {
      columns,
      rows,
    },
  });
};

export const toggleIsEditing = () => (dispatch) => {
  dispatch({
    type: TOGGLE_IS_EDITING,
  });
};

export const runGame = () => (dispatch) => {
  dispatch({ type: RUN_GAME });
};

export const nextMove = (grid) => (dispatch) => {
  dispatch({
    type: NEXT_MOVE,
    payload: {
      grid,
    },
  });
};

export const stopGame = () => (dispatch) => {
  dispatch({
    type: STOP_GAME,
  });
};

export const clearAll = () => (dispatch) => {
  dispatch({
    type: CLEAR__ALL,
  });
};
